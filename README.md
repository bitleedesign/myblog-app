**Thias is a Social Blog App 

This Social Blog App is based on Python Django framework. 
---

## Content

Here different functionalities like:

1. Creating a Blog post.
2. Create your profile.
3. Update your profile.
4. Deleting your Blog post.  the following text: *Delete this line to make a change to the README from Bitbucket.*
5. Deleting your profile.
6. And interact with others.

---

## More about me at:

https://g.Dev/eliferbit
